(defproject foppl-compiler "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [
    [org.clojure/clojure "1.8.0"]
    [org.clojure/algo.generic "0.1.3"]
    [anglican/anglican "1.0.0"]
  ]
  :jvm-opts ["-Xss4G"]
  :main ^:skip-aot hoppl.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
