(ns sampler.test-helper
    (:require [clojure.test :refer :all]
              [clojure.pprint :refer :all]
              [sampler.base :refer [sample-from-joint]]
              [sampler.core :refer [run-mh-w-gibbs run-hmc]]
              [clojure.algo.generic.functor :refer [fmap]]
              [anglican.runtime :refer [mean std]]
              [sampler.core-test :refer [progs]]
              [foppl-compiler.core :refer [compile-expr]]
              [foppl-compiler.sugar :refer [desugar-program]]
              [clojure.algo.generic.math-functions :refer :all]
              [autodiff.operations :refer [normpdf]]))

(def n-samples 500)
(def burn-in 200)

(defn evaluate-boolean [sampler code]
  (let [
      samples (sampler code n-samples burn-in)
      prob (/ ((frequencies samples) true 0) n-samples)
    ]
    (println "Prob:" prob "   -- freq: " (frequencies samples))
  )
)

(defn evaluate-mean [sampler code]
  (let [samples (sampler code n-samples burn-in)]
    (println "Mean:" (mean samples))
  )
)

(defn evaluate-mean-var [sampler code]
  (let [samples (sampler code n-samples burn-in)]
    (println "Mean:" (mean samples) "  -- StDev:" (std samples))
  )
)

;;; HW6
; (evaluate-mean run-mh-w-gibbs (nth progs 0))
; (evaluate-mean run-mh-w-gibbs (nth progs 1))
; (evaluate-boolean run-mh-w-gibbs (nth progs 2))
; (evaluate-boolean run-mh-w-gibbs (nth progs 3))
; (evaluate-mean-var run-mh-w-gibbs (nth progs 4))


;;; HW7
(evaluate-mean run-hmc (nth progs 0))
(evaluate-mean run-hmc (nth progs 1))
(evaluate-mean-var run-hmc (nth progs 5))