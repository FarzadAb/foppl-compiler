(ns sampler.base-test
    (:require [clojure.test :refer :all]
              [clojure.pprint :refer :all]
              [sampler.base :refer [sample-from-joint]]
              [foppl-compiler.core-test :refer [progs]]
              [foppl-compiler.core :refer [compile-expr]]
              [clojure.algo.generic.math-functions :refer :all]
              [autodiff.core :refer [evaluate]]
              [autodiff.operations :refer [normpdf]]))

(deftest sample-joint-test
  (let [sample (-> (nth progs 0)
          compile-expr
          :G
          sample-from-joint
        )
      symbols (keys sample)
      values (vals sample)
    ]
    (is (= 3 (count sample)) "The number of sampled variables is not correct")
    (doseq [v values]
      (is (number? v) "Sample value must be a number")
    )
    (is
      (= "S__" (subs (name (first symbols)) 0 3))
      "Wrong ordering, the first symbol must not be an observed variable")
    (is
      (= "O__" (subs (name (nth symbols 1)) 0 3))
      "Wrong ordering, the second symbol must be an observed variable")
    (is
      (= "O__" (subs (name (nth symbols 2)) 0 3))
      "Wrong ordering, the third symbol must be an observed variable")
  )
)