(ns sampler.core-test
    (:require [clojure.test :refer :all]
              [clojure.pprint :refer :all]
              [sampler.base :refer [sample-from-joint]]
              [foppl-compiler.core :refer [compile-expr]]
              [clojure.algo.generic.math-functions :refer :all]
              [autodiff.core :refer [evaluate]]
              [autodiff.operations :refer [normpdf]]))

(def progs [
  ;;; prog1
  '((let [mu (sample (normal 1 (sqrt 5)))
        sigma (sqrt 2)
        lik (normal mu sigma)]
      (observe lik 8)
      (observe lik 9)
      mu)
  )

  ;;; prog2
  '((defn observe-data [_ data slope bias]
      (let [xn (first data)
            yn (second data)
            zn (+ (* slope xn) bias)]
        (observe (normal zn 1.0) yn)
        (rest (rest data))))

    (let [slope (sample (normal 0.0 10.0))
          bias  (sample (normal 0.0 10.0))
          data (vector 1.0 2.1 2.0 3.9 3.0 5.3
                      4.0 7.7 5.0 10.2 6.0 12.9)]
      (loop 6 data observe-data slope bias)
      (vector slope bias))
  )

  ;;; prog3
  '((let [data [1.1 2.1 2.0 1.9 0.0 -0.1 -0.05]
        likes (foreach 3 []
                      (let [mu (sample (normal 0.0 10.0))
                            sigma (sample (gamma 1.0 1.0))]
                        (normal mu sigma)))
        pi (sample (dirichlet [1.0 1.0 1.0]))
        z-prior (discrete pi)
        z (foreach 7 [y data] 
            (sample z-prior)
          )]
      (foreach 7 [y data
                  beta z]
                  (observe (nth likes beta) y))
      (= (first z) (second z))
    )
  )

  ;;; prog4
  '((let [sprinkler true
          wet-grass true
          is-cloudy (sample (flip 0.5))

          is-raining (if (= is-cloudy true )
                        (sample (flip 0.8))
                        (sample (flip 0.2)))
          sprinkler-dist (if (= is-cloudy true)
                            (flip 0.1)
                            (flip 0.5))
          wet-grass-dist (if (and (= sprinkler true)
                                  (= is-raining true))
                            (flip 0.99)
                            (if (and (= sprinkler false)
                                    (= is-raining false))
                              (flip 0.0)
                              (if (or (= sprinkler true)
                                      (= is-raining true))
                                (flip 0.9))))]
      (observe sprinkler-dist sprinkler)
      (observe wet-grass-dist wet-grass)
      is-raining
    )
  )

  ;;; prog5
  '((let [x (sample (normal 0 10))
          y (sample (normal 0 10))]
      (observe (dirac (+ x y)) 7)
      [x y]
    )
  )

  ;;; prog6: actually the 3rd problem in HW7
  '((let [x (sample (normal 0 10))
        y (sample (normal 0 10))]
      (observe (normal (+ x y) 0.01) 7)
      [x y]
    )
  )
])