(ns foppl-compiler.core-test
  (:require [clojure.test :refer :all]
            [clojure.pprint :refer :all]
            [foppl-compiler.core :refer :all]
            [foppl-compiler.compiler :refer [empty-graph]]))


;;;; initial test
(deftest constant-test
  (let [eval (compile-expr '(1))]
    (and
      (is (= (:E eval) 1))           ; it should evaluate to 1
      (is (= (:G eval) empty-graph)) ; no `sample` or `observe`, hence the empty graph
    )
  )
)

(deftest partial-eval-struct-test
  (let [eval (compile-expr '(
        (let [a (vector 1 2 3) b (hash-map 1 2 3 4)]
          (vector a b))
      ))
      E (:E eval)
    ]
    (is (vector? E))
    (is (vector? (nth E 0)))
    (is (map? (nth E 1)))
  )
)

;;;; partial evaluation and variable shadowing test
;;;; Final expression should be 5 = (- (- 11 (- 2 5)) (* (Math/abs (- 2 5)) (Math/abs (- 2 5))))
(deftest partial-eval-shadow-test
  (let [eval (compile-expr '(
          (defn sqr [x] (* x x))
          (defn absqr [x] (sqr (Math/abs x)))
          (defn subt [x y] (- x y))
          (let [x 2]
            (let [x (- x 5)]
              (let [y (if (> 2 3) (+ 16 x) (- 11 x))]
                (subt y (absqr x)))
            )
          )
        )
      )]
    (is (:E eval) 5)
  )
)


;;;; edge count after partial-evaluation test
;;;; the deterministic if expression should be eliminated after partial evaluation
(deftest partial-edge-count-test
  (let [eval (compile-expr '(
      (let [mu1 (sample (normal 1 (Math/sqrt 5)))]
        (let [mu2 (sample (normal -1 (Math/sqrt 5)))]
          (let [x (sample
                    (if (> 1 2)
                      (normal mu1 1)
                      (normal mu2 1)
                    )
                  )
                ]
          x)
        )
      )
    ))]
    (is
      (= (count (:E (:G eval))) 1)
      "There should only be one edge not two")
    (is
      (= (:E eval) (first (rest (first (:E (:G eval))))))
      "The edge should be pointing towards `x` which is the output expression")
  )
)

(def progs [
    ;;; prog1
    '((let [mu (sample (normal 1 (sqrt 5)))
            sigma (sqrt 2)
            lik (normal mu sigma)]
        (observe lik 8)
        (observe lik 9)
        mu)
    )

    ;;; prog2
    '((defn observe-data [_ data slope bias]
              (let [xn (first data)
                    yn (second data)
                    zn (+ (* slope xn) bias)]
                (observe (normal zn 1.0) yn)
                (rest (rest data))))
        (let [slope (sample (normal 0.0 10.0))
                  bias  (sample (normal 0.0 10.0))
                  data (vector 1.0 2.1 2.0 3.9 3.0 5.3
                                4.0 7.7 5.0 10.2 6.0 12.9)]
        (loop 6 data observe-data slope bias)
        (vector slope bias))
    )

    ;;; prog3
    '((defn hmm-step [t states data trans-dists likes]
              (let [z (sample (get trans-dists
                                  (last states)))]
                (observe (get likes z)
                        (get data t))
                (append states z)))
        (let [data [0.9 0.8 0.7 0.0 -0.025 -5.0 -2.0 -0.1
                    0.0 0.13 0.45 6 0.2 0.3 -1 -1]
              trans-dists [(discrete [0.10 0.50 0.40])
                           (discrete [0.20 0.20 0.60])
                           (discrete [0.15 0.15 0.70])]
              likes [(normal -1.0 1.0)
                     (normal 1.0 1.0)
                     (normal 0.0 1.0)]
              states [(sample (discrete [0.33 0.33 0.34]))]
          ]
          (loop 16 states hmm-step data trans-dists likes))
    )

    ;;; prog4
    '((let [weight-prior (normal 0 1)
              W_0 (foreach 10 []
                    (foreach 1 [] (sample weight-prior)))
              W_1 (foreach 10 []
                    (foreach 10 [] (sample weight-prior)))
              W_2 (foreach 1 []
                    (foreach 10 [] (sample weight-prior)))

              b_0 (foreach 10 []
                    (foreach 1 [] (sample weight-prior)))
              b_1 (foreach 10 []
                    (foreach 1 [] (sample weight-prior)))
              b_2 (foreach 1 []
                    (foreach 1 [] (sample weight-prior)))

              x   (mat-transpose [[1] [2] [3] [4] [5]])
              y   [[1] [4] [9] [16] [25]]
              h_0 (mat-tanh (mat-add (mat-mul W_0 x)
                                    (mat-repmat b_0 1 5)))
              h_1 (mat-tanh (mat-add (mat-mul W_1 h_0)
                                    (mat-repmat b_1 1 5)))
              mu  (mat-transpose
                    (mat-tanh (mat-add (mat-mul W_2 h_1)
                                      (mat-repmat b_2 1 5))))]
          (foreach 5 [y_r y
                    mu_r mu]
          (foreach 1 [y_rc y_r
                      mu_rc mu_r]
              (observe (normal mu_rc 1) y_rc)))
          [W_0 b_0 W_1 b_1 W_2 b_2])
    )
  ]
)

;;;; graph test: counts the number of variables, edges, link functions
;;;; also makes sure the edge start vertices are correct
(deftest program1-test
  (let [eval (compile-expr (nth progs 0))]
    (let [mu (:E eval) graph (:G eval)]
      (and
        (is (= (count (:V graph)) 3) "Wrong vertex count")
        (is (= (count (:E graph)) 2) "Wrong edge count")
        (is (= (count (:L graph)) (count (:V graph))) "Wrong link-function count")
        (is (= (count (:O graph)) 2) "Wrong observation count: `a` and `b` should be observed")
        (is (every? #(= (first %) mu) (:E graph)) "All edges should be pointing out of `mu`")
      )
    )
  )
)

(deftest program2-test
  (let [eval (compile-expr (nth progs 1))
      G (:G eval)
      E (:E eval)
    ]
    (is (= (count (:V G)) 8))
    (is (= (count (:E G)) 12))
    (is (= (count E) 2))
    (let [slope (nth (vec E) 0)
          bias (nth (vec E) 1)]
      (is
        (every?
          (fn [edge] (or (= (first edge) slope) (= (first edge) bias)))
          (:E G))
        "All edges must point out from either the `slope` or the `bias`")
    )
  )
)

(deftest program3-test
  (let [eval (compile-expr (nth progs 2))
      G (:G eval)
      E (:E eval)
    ]
    (is (= (count (:V G)) 33) "Wrong number of vertices")
    (is
      (= (count (:E G)) 32)
      "Wrong number of edges found: **partial evaluation is not working properly**")
    (is (= (count (:O G)) 16) "Wrong number of observed variables")
    (is
      (= (count (:L G)) (count (:V G)))
      "The number of link functions should be the same as vertices")
  )
)

(deftest program4-test
  (let [eval (compile-expr (nth progs 3))
      E (:E eval)
      G (:G eval)
    ]
    (let [numparams (+ 1 (* 10 14))
          numsamples 5]
      (is
        (= (+ numsamples numparams) (count (:V G)))
        "The number of vertices should be equal to #samples + #params")
      (is
        (= (* numsamples numparams) (count (:E G)))
        "The number of edges should be equal to #samples * #params")
      (is
        (= numsamples (count (:O G)))
        "The data only contains 5 samples so only 5 variables should have been observed")
      (is (= (count (:V G)) (count (:L G)))
        "There should be as many link functions as the number of random variables (i.e. vertices)")
    )
  )
)
