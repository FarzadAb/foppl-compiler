(ns foppl-compiler.sugar-test
    (:require [clojure.test :refer :all]
              [foppl-compiler.sugar :refer :all]))


(deftest desugar-foreach-test
  (let [des (desugar-program '(
      (foreach 3 [x [1 2 3] y [4 5 6]] (+ x y))
    ))]
    (is (= (eval (first des)) [5 7 9]))
  )
)


;;;; a test code for desugaring let and loop
(def desugar-test1-code '(
  (defn observe-data [_ data slope bias]
      (let [xn (first data)
            yn (second data)
            zn (+ (* slope xn) bias)]
        (observe (normal zn 1.0) yn)
        (rest (rest data))))
  (let [slope (sample (normal 0.0 10.0))
          bias  (sample (normal 0.0 10.0))
          data (vector 1.0 2.1 2.0 3.9 3.0 5.3
                        4.0 7.7 5.0 10.2 6.0 12.9)]
    (loop 6 data observe-data slope bias)
    (vector slope bias))
))
;;; the solution for test1 in desugaring
(def desugar-test1-solution '(
  (defn observe-data [_ data slope bias]
      (let [xn (first data)]
        (let [yn (second data)]
          (let [zn (+ (* slope xn) bias)]
            (let [_ (observe (normal zn 1.0) yn)]
              (rest (rest data)))))))
  (let [slope (sample (normal 0.0 10.0))]
    (let [bias (sample (normal 0.0 10.0))]
      (let [data (vector 1.0 2.1 2.0 3.9 3.0 5.3 4.0 7.7 5.0 10.2 6.0 12.9)]
        (let [_
          (let [a1 slope]
            (let [a2 bias]
              (let [v0 (observe-data 0 data a1 a2)]
                (let [v1 (observe-data 1 v0 a1 a2)]
                  (let [v2 (observe-data 2 v1 a1 a2)]
                    (let [v3 (observe-data 3 v2 a1 a2)]
                      (let [v4 (observe-data 4 v3 a1 a2)]
                        (let [v5 (observe-data 5 v4 a1 a2)]
                          v5))))))))]
          (vector slope bias)))))
))
;;;; test1 for desugaring
;;;; it is a brittle test, but I couldn't find an easier way
(deftest desugar-complete-test1
  (is (= (desugar-program desugar-test1-code) desugar-test1-solution))
)

(deftest desugar-datastructures-test
  (let [des (desugar-program '(
      {:1 2 :3 4}
      [1 2 3 4]
    ))]
    (is (= (first des) '(hash-map :1 2 :3 4)))
    (is (= (first (rest des)) '(vector 1 2 3 4)))
  )
)

(deftest desugar-nested-ds-test
  (let [des (desugar-program '(
      [[1 2] [3 4]]
    ))]
    (is (= (first des) '(vector (vector 1 2) (vector 3 4))))
  )
)