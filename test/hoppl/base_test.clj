(ns hoppl.base-test
  (:require [clojure.test :refer :all]
            [clojure.pprint :refer :all]
            [hoppl.interpreter :refer [heval]]
            [hoppl.sugar :refer [desugar]]))

(defn almost-eq [a b]
  (let [eps 1e2]
    (and
      (> a (- b eps))
      (< a (+ b eps))
    )
  )
)
  
;;;; deterministic test
(deftest deterministic-fn-test
  (let [
      init-state {1 2 3 4}
      code '(
        let [
            y 10
            timesy (fn [x] (* x y))
            applytimesy (fn [x] (timesy x))
          ]
          (applytimesy 4)
      )
      [value state] (heval (desugar code) init-state {})
    ]
    (and
      (is (= value 40))              ; it should evaluate to 4 * 10 = 40
      (is (= state init-state))      ; the state should not be changed
    )
  )
)

;;;; sample test
(deftest sample-test
  (let [
      init-state {1 2 3 4}
      code '(
        let [
            d (uniform-continuous 1.7 1.9)
            y (sample d)
            timesy (fn [x] (* x y))
          ]
          (timesy 10)
      )
      [value state] (heval (desugar code) init-state {})
    ]
    (and
      (is (>= value 17))             ; the result should be between 1.7*10 = 17 and 19 = 1.9*10
      (is (<= value 19))             ; the result should be between 1.7*10 = 17 and 19 = 1.9*10
      (is (= state init-state))      ; the state should not be changed
    )
  )
)

;;;; observe test1
(deftest observe-test1
  (let [
      eps 0.1
      lprob -48.616
      code '(
        let [
          d (normal 1 0.1)
        ]
        (observe d 2)
      )
      [value {:keys [logW]}] (heval (desugar code) {} {})
    ]
    (and
      (is (= value 2))               ; the observe statement should return the observed value
      (is (almost-eq logW lprob))    ; the value of the log weight should be calculated properly
    )
  )
)

;;;; observe test2
(deftest observe-test2
  (let [
      eps 0.1
      lprob -47.232
      code '(
        let [
          d (normal 1 0.1)
        ]
        (observe d 1)
        (observe d 2)
      )
      [value {:keys [logW]}] (heval (desugar code) {} {})
    ]
    (and
      (is (= value 2))               ; the observe statement should return the observed value
      (is (almost-eq logW lprob))    ; the value of the log weight should be calculated properly
    )
  )
)