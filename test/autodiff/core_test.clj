(ns autodiff.core-test
    (:require [clojure.test :refer :all]
              [clojure.pprint :refer :all]
              [clojure.algo.generic.math-functions :refer :all]
              [autodiff.core :refer [evaluate]]
              [autodiff.operations :refer [normpdf]]))

            
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; manual tests ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def progs '(
  ;;; test1
  (fn [x] (exp (sin x)))
  ;;; test2
  (fn [x y] (+ (* x x) (sin x)))
  ;;; test3
  (fn [x] (if (> x 5) (* x x) (+ x 18)))
  ;;; test4
  (fn [x] (log x))
  ;;; test5
  (fn [x mu sigma]
    (+ (- 0 (/ (* (- x mu) (- x mu))
               (* 2 (* sigma sigma))))
       (* (- 0 (/ 1 2)) (log (* 2 (* 3.141592653589793 (* sigma sigma)))))))
  ;;; test6
  (fn [x mu sigma] (normpdf x mu sigma))
  ;;; test7
  (fn [x1 x2 x3]
    (+
      (+
        (normpdf x1 2 5)
        (if (> x2 7)
          (normpdf x2 0 1)
          (normpdf x2 10 1)))
      (normpdf x3 -4 10)
    )
  )
))

(defn almost-eq [a b]
  (let [eps (/ (max (abs b) 1e-3) 1e3)]
    (and
      (> a (- b eps))
      (< a (+ b eps))
    )
  )
)

(deftest test-prog1
  (let [res (evaluate (nth progs 0) [10])]
    (is
      (almost-eq
        (get (:grad res) 'x)
        -0.487
      )
      "df/dx should be cos(x)*exp(sin(x)) ~= -0.487"
    )
  )
)

(deftest test-prog2
  (let [res (evaluate (nth progs 1) [2 100])]
    (is
      (almost-eq
        (get (:grad res) 'x)
        3.5838
      )
      "df/dx should be 2x+cos(x) ~= 3.5838"
    )
    (is
      (almost-eq
        (get (:grad res) 'y)
        0
      )
      "df/dy should be 0"
    )
  )
)

(deftest test-prog3-1
  (let [res (evaluate (nth progs 2) [10])]
    (is
      (almost-eq
        (get (:grad res) 'x)
        20
      )
      "df/dx should be 2x = 20"
    )
  )
)

(deftest test-prog3-2
  (let [res (evaluate (nth progs 2) [4])]
    (is
      (almost-eq
        (get (:grad res) 'x)
        1
      )
      "df/dx should be 2x = 1"
    )
  )
)

(deftest test-prog4
  (let [res (evaluate (nth progs 3) [4])]
    (is
      (almost-eq
        (get (:grad res) 'x)
        0.25
      )
      "df/dx should be 1/x = 0.25"
    )
  )
)

(deftest test-prog6
  (let [res (evaluate (nth progs 5) [1 2 3])]
    (is
      (almost-eq
        (get (:grad res) 'x)
        0.111111
      )
      "df/dx should be ~= 0.111111 = (μ-x) / (σ**2)"
    )
    (is
      (almost-eq
        (get (:grad res) 'mu)
        -0.111111
      )
      "df/dμ should be ~= -0.111111 = (x-μ) / (σ**2)"
    )
    (is
      (almost-eq
        (get (:grad res) 'sigma)
        -0.296296
      )
      "df/dσ should be ~= -0.296296 = (μ**2 - 2*μ*x - σ**2 + x**2) / (σ**3)"
    )
  )
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; automatic tests ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn addd [exprl i d]
  (if (= i 0)
    (reduce conj [`(~'+ ~d ~(first exprl))] (subvec exprl 1))
    (reduce conj (subvec exprl 0 i)
            (reduce conj [`(~'+ ~d ~(get exprl i))] (subvec exprl (+ i 1))))))
  
(defn finite-difference-expr [expr args i d]
  `(~'/ (~'- (~expr ~@(addd args i d)) (~expr ~@(addd args i `(- ~d)))) (* 2 ~d)))

(defn finite-difference-grad [expr]
  (let [[op args body] expr
        d (gensym)
        fdes (map #(finite-difference-expr expr args % d) (range (count args)))
        argsyms (map (fn [x] `(~'quote ~x)) args)]
    `(~'fn [~@args]
        (~'let [~d 0.001]
          ~(zipmap argsyms fdes)))))

(defn test-expr-with-args [expr args]
  (require '[clojure.algo.generic.math-functions :refer :all])
  (require '[autodiff.operations :refer [normpdf]])
  (let [
        fd-grad (apply (eval (finite-difference-grad expr)) args)
        prog (evaluate expr args)
        prog-grad (:grad prog)
        value (:val prog)
    ]
    (is (almost-eq value (apply (eval expr) args)) "Wrong value")
    (doseq [sym (vec (keys fd-grad))]
      (is
        (almost-eq (get prog-grad sym) (get fd-grad sym))
        (str "Wrong grad for '" sym "' with args " args " in " expr "")
      )
    )
  )
)

;;; TODO: add automatic tests

;;; random tests

(deftest test-prog5-1
  (test-expr-with-args (nth progs 4) [5 5 2])
)

(deftest test-prog5-2
  (test-expr-with-args (nth progs 4) [5 10 2])
)

(deftest test-prog7-1
  (test-expr-with-args (nth progs 4) [9 6 12])
)

(deftest test-prog7-2
  (test-expr-with-args (nth progs 4) [4 8 3])
)

;;; final tests
(deftest test-prog1-final
  (test-expr-with-args (nth progs 0) [0])
)
(deftest test-prog2-final
  (test-expr-with-args (nth progs 1) [0 10])
)
(deftest test-prog3-final
  (let [res (evaluate (nth progs 2) [5.000001])]
    (is
      (almost-eq
        (get (:grad res) 'x)
        10.000002
      )
      "df/dx should be 2x = 10.000002"
    )
    (is
      (almost-eq
        (:val res)
        25.00001
      )
      "value should be x**2 = 25.00001"
    )
  )
)
(deftest test-prog4-final
  (test-expr-with-args (nth progs 3) [0.1])
)
(deftest test-prog5-final
  (test-expr-with-args (nth progs 4) [10 0 2])
)
(deftest test-prog6-final
  (test-expr-with-args (nth progs 5) [10 0 2])
)
(deftest test-prog7-final
  (test-expr-with-args (nth progs 6) [2 7.01 5])
)
