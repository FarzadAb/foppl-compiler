(ns autodiff.test-helper
  (:require [autodiff.core-test :refer [progs]])
  (:require [autodiff.core :refer [evaluate]])
)

(println "prog1-final" (evaluate (nth progs 0) [0]))

(println "prog2-final" (evaluate (nth progs 1) [0 10]))

(println "prog3-final" (evaluate (nth progs 2) [5.000001]))

(println "prog4-final" (evaluate (nth progs 3) [0.1]))

(println "prog5-final" (evaluate (nth progs 4) [10 0 2]))

(println "prog6-final" (evaluate (nth progs 5) [10 0 2]))

(println "prog7-final" (evaluate (nth progs 6) [2 7.01 5]))

