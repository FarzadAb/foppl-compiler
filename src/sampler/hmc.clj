(ns sampler.hmc
    (:require [clojure.set :refer [union]])
    (:require [clojure.pprint :refer [pprint]])
    (:require [clojure.algo.generic.math-functions :refer [exp]])
    (:require [clojure.walk :refer [prewalk-replace]])
    (:require [clojure.core.matrix :refer [add sub mul div inverse mmul transpose mget magnitude]])
    (:require [clojure.core.matrix.linear :refer [cholesky]])
    (:require [foppl-compiler.compiler :refer [free-vars]])
    (:require [autodiff.autodiff :refer [evaluate-fn]])
    (:require [anglican.runtime :refer [
      observe* sample* distribution normal uniform-discrete
      beta uniform-continuous discrete flip
    ]])
    (:require [anglican.stat :refer [empirical-covariance]])
    (:require [sampler.base :refer [sample-from-prior]])
    (:require [utils.christian :refer [
      observe->sample observes->dists observe->dist eval-expr create-instr-funcs
      graph->instructions instr->logprob
    ]])
    (:use [foppl-compiler.core])
    (:require [clojure.inspector :refer [atom?]])
    (:gen-class))

(defn leapfrog [x0 p0 ∇]

  (def ϵ 0.02)
  (def T 40)

  (loop [
      ph (sub p0 (div (mul ϵ (∇ x0)) 2))  ;; momentum: p_{t-1/2}
      xl x0                               ;; sample:   x_{t-1}
      t 1                                 ;; time index
    ]
    (if (= t T)
      (let [
          xt (add xl (mul ϵ ph))
          pt (sub ph (div (mul ϵ (∇ xt)) 2))
        ]
        [xt pt]
      )
      (let [
          xt (add xl (mul ϵ ph))
          php (sub ph (mul ϵ (∇ xt)))     ;; p_{t+1/2}
        ]
        (recur php xt (+ 1 t))
      )
    )
  )
)

(defn calc-energy [x p γ Minv]
  ; (println "Kin:" p "->" (mget (mmul (transpose p) Minv p)))
  (+
    (/ (mget (mmul (transpose p) Minv p)) 2)  ;; Kinetic Energy: 1/2 P^t M^{-1} P
    (- (γ x))                                 ;; Potential Energy: - log γ(x)
                                              ;; in this code γ refers to the log probability
  )
)

(defn hmc-step [x0 ∇ γ Minv Mhalf]
  (let [
      norm-sample (mapv (fn [_] (sample* (normal 0 1))) x0)
      p0 (mmul Mhalf norm-sample)
      ; _ (println "Momentum:" norm-sample p0)
      [xt, pt] (leapfrog x0 p0 ∇)
      e-l (calc-energy x0 p0 γ Minv)
      e-p (calc-energy xt pt γ Minv)
      e-diff (- e-l e-p)
      u (sample* (uniform-continuous 0 1))
      ; _ (println "step:" x0 xt (γ x0) (γ xt) e-diff)
    ]
    (if (< u (exp e-diff))
      xt
      x0
    )
  )
)

(defn calc-cov-matrix [graph x-keys γ]
  ; (let [w (for [_ (range 1000)]
  ;           (let [
  ;               s (sample-from-prior graph)
  ;               v (mapv s x-keys)
  ;             ]
  ;             [v (γ v)]
  ;           )
  ;         )]
  ;   (empirical-covariance w)
  ; )
  (vec (for [s1 x-keys]
    (vec (for [s2 x-keys]
      (if (= s1 s2) 1 0)
    ))
  ))
)

(defn post-grad [graph args]
  (let [
      normalize (fn [x] (if (> (magnitude x) 100) (div x (* 0.01 (magnitude x))) x))
      raw-instrs (->> graph
          graph->instructions
          (map (fn [[s i]] (instr->logprob i s)))
          (reduce #(list '+ %1 %2))
          (list '- 0)
        )
      no-obs-instrs (prewalk-replace (:O graph) raw-instrs)
    ]
    (fn [x]
      (-> (evaluate-fn no-obs-instrs (vec args) (vec x))
        :grad
        (map args)
        vec
        normalize
      )
    )
  )
)

(defn hmc [graph num-samples burn-in]
  (let [
      total (+ num-samples burn-in)

      x0 (sample-from-prior graph)
      x-keys (keys x0)

      ∇ (post-grad graph x-keys)

      log-probs (vals (create-instr-funcs graph identity))
      x-list-to-hashmap (fn [x] (into (:O graph) (map #(vector %1 %2) x-keys x)))
      γ (fn [x]
          (let [hmx (x-list-to-hashmap x)]
            (apply + (map #(% hmx) log-probs))
          )
        )
      
      Minv (calc-cov-matrix graph x-keys γ)

      M (inverse Minv)

      Mhalf (:L (cholesky M))
    ]
    (loop [
        samples [(vec (vals x0))]
        ind 0
      ]
      (if (= ind total)
        (mapv x-list-to-hashmap (subvec samples (+ 1 burn-in)))
        (recur
          (conj samples (hmc-step (peek samples) ∇ γ Minv Mhalf))
          (+ ind 1))
      )
    )
  )
)
