(ns sampler.mh-w-gibbs
    (:require [clojure.set :refer [union]])
    (:require [clojure.pprint :refer [pprint]])
    (:require [clojure.algo.generic.math-functions :refer [exp]])
    (:require [foppl-compiler.compiler :refer [free-vars]])
    (:require [anglican.runtime :refer [
      observe* sample* distribution normal uniform-discrete
      beta uniform-continuous discrete flip
    ]])
    (:require [sampler.base :refer [sample-from-prior]])
    (:require [utils.christian :refer [
      observe->sample observes->dists observe->dist eval-expr create-instr-funcs
    ]])
    (:use [foppl-compiler.core])
    (:require [clojure.inspector :refer [atom?]])
    (:gen-class))


(defn sample-from-link [graph x values-map]
  (-> graph
    :L
    (get x)
    observe->sample
    (eval-expr (vec values-map))
  )
)

(defn logprob-from-link
  ([graph x values-map c-value]
    (logprob-from-link graph x (assoc values-map x c-value))
  )
  ([graph x values-map]
    (
      (-> graph
        :logprobs
        (get x)
      )
      values-map
    )
  )
)


(defrecord q-prior [graph x c-sample]
  Object
    (toString [this]
      (str "Q(" (:x this) ")"))
  distribution
    (sample* [this]
      (-> this
        :graph
        :L
        (get (:x this))
        observe->sample
        (eval-expr (vec c-sample))
      )
    )
    (observe* [this var]
      (-> this
        :graph
        :L
        (get (:x this))
        (eval-expr
          (vec (assoc c-sample (:x this) var))
        )
      )
    )
)

(defn accept-ratio [graph x q-dist qp-dist prev prop]
  (let [
      neighbors (->> graph
                  :E
                  (filter #(= x (first %)))
                  (map second)
                  (cons x)
                )
      prev-m-obs (merge prev (:O graph))
      prop-m-obs (merge prop (:O graph))
    ]
    (-
      (apply +
        (observe* qp-dist (prev x))
        (map #(logprob-from-link graph % prop-m-obs) neighbors)
      )
      (apply +
        (observe* q-dist  (prop x))
        (map #(logprob-from-link graph % prev-m-obs) neighbors)
      )
    )
  )
)


(defn get-q-prior [graph x c-sample]
  ; (q-prior. graph x c-sample)
  (let [
      dist-expr (-> graph :L (get x) observe->dist)
      ;;; hacky way to do this. I don't know a better way
      dist-name (first dist-expr)
    ]
    (cond
      ; (= dist-name 'flip)
      ;   ; (flip 0.5)
      ;   ((-> graph :dists (get x)) c-sample)
      (= dist-name 'dirac)
        (normal (c-sample x) 0.2)
      (= dist-name 'normal)
        (normal (c-sample x) 1)
      :else
        ((-> graph :dists (get x)) c-sample)
      ; (= dist-name 'dirichlet)
      ;   ((-> graph :dists (get x)) c-sample)
      ; (= dist-name 'discrete)
      ;   ((-> graph :dists (get x)) c-sample)
      ; (= dist-name 'gamma)
      ;   ((-> graph :dists (get x)) c-sample)
      ; :else
      ;   (normal (c-sample x) 1)
    )
  )
)


(defn mh-w-gibbs-step-i [graph x prev]
  (let [
      ; _ (println "hello")
      q (get-q-prior graph x prev)
      ; _ (println (str q))
      prop (assoc prev x (sample* q))
      ; _ (println prop)
      qp (get-q-prior graph x prop)
      ; _ (println "\n\n\n\n**** " q qp)
      ratio (accept-ratio graph x q qp prev prop)
      u (sample* (uniform-continuous 0 1))
    ]
    (if (< u ratio)
      prop
      prev)
  )
)

(defn mh-w-gibbs-step [graph prev]
  (loop [
      x (first (keys prev))
      remaining (rest (keys prev))
      current prev
    ]
    (if (= nil x)
      current
      (recur
        (first remaining)
        (rest remaining)
        (mh-w-gibbs-step-i graph x current))
    )
  )
)

(defn mh-w-gibbs-sampler [graph num-samples burn-in]
  (let [
      total (+ num-samples burn-in)
      log-probs (create-instr-funcs graph identity)
      dist-funcs (create-instr-funcs graph observes->dists)
      graph (-> graph
              (assoc :logprobs log-probs)
              (assoc :dists dist-funcs)
            )
      ; _ (println graph)
    ]
    (loop [
        samples [(sample-from-prior graph)]
        ind 0
      ]
      (if (= ind total)
        (subvec samples (+ 1 burn-in))
        (recur
          (conj samples (mh-w-gibbs-step graph (peek samples)))
          (+ ind 1))
      )
    )
  )
)


; (def gg (:G (compile-expr '(
;   (let [mu (sample (normal 1 (sqrt 5)))
;         sigma (sqrt 2)
;         lik (normal mu sigma)]
;     (observe lik 8)
;     (observe lik 9)
;     mu)
; ))))


; (create-funcs gg)

; (def gg (:G (compile-expr '((defn observe-data [_ data slope bias]
;     (let [xn (first data)
;           yn (second data)
;           zn (+ (* slope xn) bias)]
;       (observe (normal zn 1.0) yn)
;       (rest (rest data))))

;   (let [slope (sample (normal 0.0 10.0))
;         bias  (sample (normal 0.0 10.0))
;         data (vector 1.0 2.1 2.0 3.9 3.0 5.3
;                     4.0 7.7 5.0 10.2 6.0 12.9)]
;     (loop 6 data observe-data slope bias)
;     (vector slope bias)))
; )))

; gg

; (doseq [X (mh-w-gibbs-sampler gg 200 10)] (println X))
