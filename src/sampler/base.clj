(ns sampler.base
    (:require [clojure.set :refer [union]])
    (:require [foppl-compiler.compiler :refer [free-vars]])
    (:require [utils.base :refer [map-add topol-sort]])
    (:require [utils.christian :refer [graph->instructions observes->samples eval-instructions]])
    (:require [anglican.runtime :refer [observe* sample* distribution normal uniform-discrete beta uniform-continuous discrete]])
    (:require [foppl-compiler.core :refer [compile-expr]])
    (:require [clojure.inspector :refer [atom?]])
    (:gen-class))

(defn sample-from-joint [graph]
  (into {}
    (-> graph
        graph->instructions
        observes->samples
        eval-instructions
    )
  )
)

(defn sample-from-prior [graph]
  (into {}
    (filter
      #(not (contains? (:O graph) (first %)))
      (-> graph
          graph->instructions
          observes->samples
          eval-instructions
      )
    )
  )
)
