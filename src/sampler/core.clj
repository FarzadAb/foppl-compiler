(ns sampler.core
      (:require [clojure.set :refer [union]])
      (:require [foppl-compiler.core :refer [compile-expr]])
      (:require [sampler.mh-w-gibbs :refer [mh-w-gibbs-sampler]])
      (:require [sampler.hmc :refer [hmc]])
      (:require [utils.christian :refer [eval-expr]])
      (:require [clojure.inspector :refer [atom?]])
      (:gen-class))


(defn run-mh-w-gibbs
  ([code num-samples]
    (run-mh-w-gibbs code num-samples 100)
  )
  ([code num-samples burn-in]
    (let [
        eval (compile-expr code)
        samples (mh-w-gibbs-sampler (:G eval) num-samples burn-in)
        return (:E eval)
      ]
      (map #(eval-expr return %) samples)
    )
  )
)

(defn run-hmc
  ([code num-samples]
    (run-hmc code num-samples 100)
  )
  ([code num-samples burn-in]
    (let [
        eval (compile-expr code)
        samples (hmc (:G eval) num-samples burn-in)
        return (:E eval)
      ]
      (map #(eval-expr return %) samples)
    )
  )
)

