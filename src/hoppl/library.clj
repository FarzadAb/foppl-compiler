(ns hoppl.library
  (:require [clojure.set :refer [union]])
  (:use [anglican core runtime emit stat])
  (:require [clojure.inspector :refer [atom?]]))

;;;; allows the addition of predefined functions for the user to use
(def predef-funs '(
  (defn reduce [f x coll]
    (if (empty? coll) x (reduce f (f x (first coll)) (rest coll)))
  )
))


;;;; Transform the primitives so that they take the "STATE" as input
;;; and return it without modifying it
(defn fix-primitive [func]
  (fn [stt & args]
    (list (apply func args) stt)
  )
)


(def primitives {
  '+ (fix-primitive +)
  '- (fix-primitive -)
  '* (fix-primitive *)
  '/ (fix-primitive /)
  '< (fix-primitive <)
  '> (fix-primitive >)
  '= (fix-primitive =)
  'log (fix-primitive log)
  'sqrt (fix-primitive sqrt)
  'flip (fix-primitive flip)
  'empty? (fix-primitive empty?)
  'first (fix-primitive first)
  'rest (fix-primitive rest)
  'get (fix-primitive get)
  'conj (fix-primitive conj)
  'peek (fix-primitive peek)
  'vector (fix-primitive vector)
  'hash-map (fix-primitive hash-map)
  'uniform-continuous (fix-primitive uniform-continuous)
  'normal (fix-primitive normal)
  'discrete (fix-primitive discrete)
})