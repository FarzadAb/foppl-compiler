(ns hoppl.sugar
  (:require [clojure.inspector :refer [atom?]]))

;;;; pre-declaring desugar because of the cyclic dependencies
(declare desugar)


;;;; desugars a let block that possibly contains multiple variable assignments
(defn desugar-let [[letblock & exprs]]
  (let [vnames (take-nth 2 letblock)
        vexprs (map desugar (take-nth 2 (rest letblock)))
        [e & extra] (reverse (map desugar exprs))
        vnames (concat vnames (map (fn [x] '_) extra))
        vexprs (concat vexprs (reverse extra))
    ]
    (loop [e e
           vnames (reverse vnames)
           vexprs (reverse vexprs)]
      (if (empty? vnames)
        e
        (recur
          (list (list 'fn [(first vnames)] e) (first vexprs))
          (rest vnames)
          (rest vexprs)
        )
      )
    )
  )
)

;;;; desugars an expression or function definition
(defn desugar [expr]
  (cond
    (vector? expr) (cons 'vector (seq (map desugar expr)))
    (map? expr) (cons 'hash-map (mapcat (fn [x] [(first x) (desugar (nth x 1))]) expr))
    (seq? expr)
      (let [[f & r] expr]
        (cond
          (= f 'let) (desugar-let r)
          (contains? #{'defn 'fn} f) (concat (butlast expr) [(desugar (last expr))])
        ;   (= f 'loop) (desugar-loop r)
          :else (map desugar expr)
        )
      )
    :else expr
  )
)


;;;; desugars the entire program
;;; replaces all `defn` statements with a giant `let` block and calls `desugar` on the result
(defn desugar-program [expr]
  (let [
      body (last expr)
      df-blocks (butlast expr)
      fnames (map second df-blocks)
      ftransformed (map #(cons 'fn (rest %)) df-blocks)
      fixed (if (empty? df-blocks)
              body
              (list
                'let
                (vec (apply concat (map list fnames ftransformed)))
                body
              )
            )
    ]
    (desugar fixed)
  )
)

