(ns hoppl.core
  (:require [clojure.set :refer [union]])
  (:require [anglican.stat :refer [empirical-mean empirical-variance]])
  (:require [hoppl.interpreter :refer [heval]])
  (:require [hoppl.sugar :refer [desugar-program]])
  (:require [hoppl.library :refer [predef-funs]])
  (:require [clojure.inspector :refer [atom?]]))


;;;; performs importance sampling
(defn imp-sampling [nb-samples code]
  (let [
      expr (desugar-program (concat predef-funs code))
      samples (for [_ (range nb-samples)] (heval expr {:logW 0} {}))
      weighted (map #(list (first %) (:logW (second %))) samples)
    ]
    [(empirical-mean weighted) (empirical-variance weighted)]
  )
)
