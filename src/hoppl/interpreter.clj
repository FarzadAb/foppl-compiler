(ns hoppl.interpreter
    (:require [clojure.set :refer [union]])
    (:require [hoppl.sugar :refer [desugar desugar-program]])
    (:require [hoppl.library :refer [primitives]])
    (:use [anglican core runtime emit stat])
    (:require [clojure.inspector :refer [atom?]]))

;;;; the name stands for HOPPL eval, don't want it to clash with `clojure.core$eval`
(declare heval)

(defn heval-if [[e1 e2 e3] state vars]
  (let [[r1 s1] (heval e1 state vars)]
    (if r1
      (heval e2 s1 vars)
      (heval e3 s1 vars)
    )
  )
)

(defn heval-multiple [args s0 vars]
  (loop [args args       ; e_i 's
         results (list)  ; r_i 's in the reverse order
         state s0]       ; the value of σ which will be updated at each step
    (if (empty? args)
      (list (reverse results) state)
      (let [[ri si] (heval (first args) state vars)]
        (recur
          (rest args)
          (cons ri results)
          si)
      )
    )
  )
)

(defn heval-application [exprs state vars]
  (let [
      [[f & args] state] (heval-multiple exprs state vars)
    ]
    (apply f state args)
  )
)



(defn heval-fn [[fname argnames expr] state vars]
  (list
    (fn self [stt & args]
      (heval
        expr
        stt
        (assoc
          (into vars (mapv #(vector %1 %2) argnames args))
          fname
          self
        )
      )
    )
    state
  )
)

(defn heval-sample [expr state vars]
  (let [
      [dist stt] (heval expr state vars)
      s (sample* dist)
    ]
    (list s stt)
  )
)

(defn heval-observe [[dist-e val-e] state vars]
  (let [
      [dist stt1] (heval dist-e state vars)
      [val  stt2] (heval val-e  stt1  vars)
      lprob (observe* dist val)
    ]
    (list
      val
      (assoc stt2 :logW (+ lprob (:logW stt2 0))))
  )
)

;;;; HOPPL eval
;;; evaluates the HOPPL code and reports the importance weight
;;; output format: (list <return-value> {:logW <weight>})
(defn heval [expr state vars]
  (if (not (seq? expr))
    (cond
      (contains? vars expr) (list (vars expr) state)
      (contains? primitives expr) (list (primitives expr) state)
      :else (list expr state)
    )
    (cond
      (= 'if (first expr)) (heval-if (rest expr) state vars)
      (= 'fn (first expr))
        (if (= 3 (count expr))
          (heval-fn (cons (gensym) (rest expr)) state vars) ;; creates random name for the function
          (heval-fn (rest expr) state vars))
      (= 'sample (first expr)) (heval-sample (second expr) state vars)
      (= 'observe (first expr)) (heval-observe (rest expr) state vars)
      :else (heval-application expr state vars)
    )
  )
)
