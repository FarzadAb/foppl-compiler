;;; Adapted from Christian Weilbach's codebase
;;; https://gitlab.com/whilo/foppl-compiler/blob/master/src/foppl_compiler/core.clj

(ns utils.christian
    (:require [clojure.set :refer [union]])
    (:require [utils.base :refer [topol-sort]])
    (:gen-class))

;;; given a graph returns the link functions
(defn graph->instructions [graph]
  (vec
    (for [t (topol-sort graph)]
      [t ((:L graph) t)])
  )
)

;;; given an expression containing observes transforms it to the distribution itself
(defn observe->dist [expr]
  (if (= (first expr) 'if)
    (let [[_ criteria [_ dist _] _] expr]
      dist
    )
    (let [[_ dist _] expr]
      dist
    )
  )
)

;;; given an expression containing observes transforms it
;;; so that it can be sampled from
(defn observe->sample [expr]
  (list 'sample* (observe->dist expr))
)

;;; helper function that transforms a set of instructions to be sampled
(defn observes->samples [instructions]
  (mapv (fn [[sym expr]] [sym (observe->sample expr)]) instructions)
)

(defn observes->dists [instructions]
  (mapv (fn [[sym expr]] [sym (observe->dist expr)]) instructions)
)

;;; evaluates an expression in the correct namespace, i.e. "foppl-compiler.primitives"
(defn safe-eval [expr]
  (binding [*ns* (find-ns 'foppl-compiler.primitives)]
    (eval expr)
  )
)

;;; evaluates an expression given a context, i.e. variable values
(defn eval-expr [expr context]
  (let [scope `(fn [{:syms ~(vec (take-nth 2 (apply concat context)))}] ~expr)]
    ((safe-eval scope) (into {} context))
  )
)

;;; evaluates the instructions
(defn eval-instructions [instructions]
  (reduce
    (fn [acc [s v]]
        (conj acc [s (eval-expr v acc)])
    )
    []
    instructions
  )
)

(defn instr->func [[sym instr] arguments]
  (let [fn-expr (list 'fn [{:syms arguments}] instr)]
    [sym (safe-eval fn-expr)]
  )
)

;;; a trick to make things faster by evaluating everything only once
(defn instrs->funcmap [instructions vertices]
  (->> instructions
    (map #(instr->func % vertices))
    (into {})
  )
)

(defn create-instr-funcs [graph transform]
  (-> graph
    graph->instructions
    transform
    (instrs->funcmap (vec (:V graph)))
  )
)

(defn instr->logprob [instr val]
  (cond
    (not (seq? instr)) instr
    (= 'sample* (first instr)) (instr->logprob (second instr) val)
    (= 'observe* (first instr)) (instr->logprob (second instr) (nth instr 2))
    (= 'normal (first instr)) (cons 'normpdf (cons val (rest instr)))
    (= 'flip (first instr)) (cons 'bernpdf (cons val (rest instr)))
    ;;; TODO: add more distributions as needed
    :else (cons (first instr) (map #(instr->logprob % val) (rest instr)))
  )
)