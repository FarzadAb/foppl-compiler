(ns utils.base
    (:gen-class))

(defn map-add [dict keys vals]
  (loop [dict dict
          keys keys
          vals vals]
    (if (empty? keys)
      dict
      (let [key (first keys)
            prev-val (get dict key 0)
          ]
        (recur
          (assoc dict key (+ prev-val (first vals)))
          (rest keys)
          (rest vals)
        )
      )
    )
  )
)

;;; topological sort
;;; input must be a graph: {:V vertices :E edges}
(defn topol-sort [{:keys [V E]}]
  (let [
      child-list (apply merge-with into (map (fn [x] {(first x) [(second x)]}) E))
    ]
    (loop [
        fins (frequencies (map second E))
        no-pars (filter #(not (contains? fins %)) V)
        x (first no-pars)
        sorted []
      ]
      (if (empty? no-pars)
        sorted
        (let [
            x-children (child-list x [])
            new-fins (map-add fins x-children (repeat -1))
            finished-vars (filter #(= 0 (new-fins %)) x-children)
            new-no-pars (concat (rest no-pars) finished-vars)
          ]
          (recur
            new-fins
            new-no-pars
            (first new-no-pars)
            (conj sorted x)
          )
        )
      )
    )
  )
)
