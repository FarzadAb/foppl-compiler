(ns autodiff.core
    (:require [clojure.pprint :refer :all])
    (:require [autodiff.autodiff :refer [read-fn]])
    (:gen-class))

(defn evaluate [fn-expr args]
  (apply (read-fn fn-expr) (vec args))
)
