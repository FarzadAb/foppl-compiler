(ns autodiff.operations
    (:require [clojure.set :refer [union]])
    (:require [clojure.algo.generic.math-functions :refer :all])
    (:require [anglican.runtime :refer [observe* normal flip]])
    (:require [clojure.inspector :refer [atom?]])
    (:gen-class))

(def ops
  (reduce
    merge
    (list
      {'* {:f * :d [(fn [a b] b) (fn [a b] a)]}}
      ; f(a,b) = a * b <-> (* a b)
      ; df/da = b
      ; df/db = a

      {'- {:f - :d [(fn [a b] 1) (fn [a b] -1)]}}
      ; f(a,b) = a - b <-> (- a b)
      ; df/da = 1
      ; df/db = -1

      {'+ {:f + :d [(fn [a b] 1) (fn [a b] 1)]}}
      ; f(a,b) = a + b <-> (+ a b)
      ; df/da = 1
      ; df/db = 1

      {'/ {:f / :d [(fn [a b] (/ 1 b)) (fn [a b] (* a (/ -1 (* b b))))]}}
      ; f(a,b) = a / b <-> (/ a b)
      ; df/da = 1
      ; df/db = -1/b^2

      {'exp {:f exp :d [(fn [a] (exp a))]}}
      ; f(a) = (exp a)
      ; df/da = (exp a)
      {'relu {:f #(max 0 %) :d [(fn [a] (if (> a 0) 1 0))]}}
      ; f(a) = (relu a)
      ; df/da = 1 if a > 0, 0 otherwise

      {'log {:f log :d [(fn [a] (/ 1 a))]}}
      ; f(a) = (log a)
      ; df/da = 1/a

      {'sqrt {:f sqrt :d [(fn [a] (/ 1 (* 2 (sqrt a))))]}}
      ; f(a) = (sqrt a)
      ; df/da = 1/(2 * sqrt(x))

      {'sin {:f sin :d [(fn [a] (cos a))]}}
      ; f(a) = (sin a)
      ; df/da = (cos a)

      {'cos {:f cos :d [(fn [a] (- (sin a)))]}}
      ; f(a) = (cos a)
      ; df/da = -(sin a)

      {'< {:f < :d [(fn [a b] 0) (fn [a b] 0)]}}
      {'> {:f > :d [(fn [a b] 0) (fn [a b] 0)]}}
      {'= {:f = :d [(fn [a b] 0) (fn [a b] 0)]}}
    )
  )
)

(defn normpdf [y m s]
  (observe* (normal m s) y))

(defn bernpdf [y p]
  (observe* (flip p) y))

(def funcs {
  ;;;; PDF for the normal distribution
  'normpdf {
    :args ['x 'mu 'sigma]
    :code '(-
        (-
          0
          (/
            (* (- x mu) (- x mu))
            (* 2 (* sigma sigma))
          )
        )
      (/
        (log (* 2 (* 3.141592653589793 (* sigma sigma))))
        2
      )
    )
  }
  'bernpdf {
    :args ['x 'p]
    :code '(if (= x true) (log p) (log (- 0 p)))
  }
})
