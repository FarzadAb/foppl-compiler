(ns autodiff.autodiff
    (:require [clojure.set :refer [union]])
    (:require [clojure.pprint :refer :all])
    (:require [clojure.walk :refer [prewalk-replace]])
    (:require [utils.base :refer [map-add]])
    (:use [autodiff.operations])
    (:require [clojure.inspector :refer [atom?]])
    (:gen-class))

(defn make-empty-graph [args values]
  (into
    {}
    (map
        (fn [k v]
          [k {:rev nil :args [] :fout 0 :val v}]
        )
        args
        values
    )
  )
)

(declare forward-expr)

(defn forward-var [expr graph]
  {:val (:val (get graph expr))
   :sym expr
   :graph graph}
)

(defn forward-const [expr graph]
  (let [sym (gensym "C__")]
    {:val expr
     :sym sym
     :graph (assoc graph sym {:rev nil :args [] :fout 0 :val expr})
    }
  )
)

(defn forward-if [expr graph]
  (let [e1 (nth expr 1)
        e2 (nth expr 2)
        e3 (nth expr 3)
        fe1 (forward-expr e1 graph)
        cfb-expr (if (:val fe1) e2 e3)]
    (forward-expr cfb-expr graph)
  )
)

(defn forward-multi-exprs [exprs graph]
  (loop [
      graph graph
      e_vec []
      ei (first exprs)
      remaining (rest exprs)
    ]
    (if (= ei nil)
      {:e_vec e_vec :graph graph}
      (let [f_ei (forward-expr ei graph)
            sym (:sym f_ei)
            graph (:graph f_ei)
          ]
        (recur
          graph
          (conj e_vec sym)
          (first remaining)
          (rest remaining)
        )
      )
    )
  )
)

(defn update-fouts [graph keys updates]
  (if (empty? keys)
    graph
    (let [sym (first keys)
          {:keys [rev args fout val]} (get graph sym)
        ]
      (update-fouts
        (assoc
          graph
          sym
          {:rev rev :args args :fout (+ fout (first updates)) :val val}
        )
        (rest keys)
        (rest updates)
      )
    )
  )
)

(defn forward-op [expr graph]
  (let [op (get ops (first expr))
        {:keys [e_vec graph]} (forward-multi-exprs (rest expr) graph)
        graph (update-fouts graph (seq e_vec) (repeat 1))
        val (apply
              (:f op)
              (map #(:val (get graph %)) e_vec))
        sym (gensym "V__")]
    {:val val
     :sym sym
     :graph (assoc graph sym {:rev (:d op) :args e_vec :fout 0 :val val})
    }
  )
)

(defn forward-func [expr graph]
  (let [f (get funcs (first expr))
        {:keys [e_vec graph]} (forward-multi-exprs (rest expr) graph)
        replace-map (into {} (map #(vector %1 %2) (:args f) e_vec))
        f-expr (prewalk-replace replace-map (:code f))]
    ; (println "--->  " f-expr)
    (forward-expr f-expr graph)
  )
)

(defn forward-expr [expr graph]
  (cond
    (atom? expr) (if (contains? graph expr)
                    (forward-var expr graph)
                    (forward-const expr graph))
    (= 'if (first expr)) (forward-if expr graph)
    (contains? funcs (first expr)) (forward-func expr graph)
    :else (forward-op expr graph)
  )
)

(defn reverse-mode [graph final-sym]
  (loop [
      fouts (into {} (map (fn [x] [(first x) (:fout (second x))]) graph))
      grads {final-sym 1}
      queue (list final-sym)
    ]
    (if (empty? queue)
      grads
      (let [sym (first queue)
            sym-grad (get grads sym)
            poped-queue (rest queue)
            {:keys [rev args]} (get graph sym)
            inputs (map #(:val (get graph %)) args)
            pds (map #(apply % inputs) rev)
            new-fouts (map-add fouts args (repeat -1))
            finished-vars (filter #(= 0 (get new-fouts %)) args)
          ]
        (recur
          new-fouts
          (map-add grads args (map * pds (repeat sym-grad)))
          (concat poped-queue finished-vars)
        )
      )
    )
  )
)

(defn evaluate-fn [expr args values]
  (let [f (forward-expr
            expr
            (make-empty-graph args values))
        grads (reverse-mode (:graph f) (:sym f))
        grads-fixed (merge (into {} (map (fn [a] [a 0]) args)) grads)
    ]
    {:val (:val f)
     :grad (into {} (map (fn [a] [a (get grads-fixed a)]) args))}
  )
)

(defn read-fn [fn-expr]
    (let [args (nth fn-expr 1)
          expr (nth fn-expr 2)
        ]
      (fn [& values] (evaluate-fn expr args (vec values)))
    )
)

