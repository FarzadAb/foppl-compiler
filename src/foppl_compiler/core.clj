(ns foppl-compiler.core
  (:require [clojure.set :refer [union]])
  (:use [foppl-compiler.primitives])
  (:use [foppl-compiler.compiler])
  (:use [foppl-compiler.sugar])
  (:require [anglican.runtime :refer [observe* sample* normal]])
  (:require [clojure.inspector :refer [atom?]])
  (:gen-class))


; (defn -main
;   "I don't do a whole lot ... yet."
;   [& args]
;   (println "Hello, World!"))

;;;; helper function to compile a program
(defn compile-expr [e]
  (read-program {} 'true {} (desugar-program e))
)
