(ns foppl-compiler.primitives
    (:require [clojure.core.matrix :as m])
    (:require [anglican.runtime :refer [
        exp tanh sqrt flip defdist observe* sample* normal gamma dirichlet discrete
    ]]))
    (defn append [& args] (apply conj args))
    (defn mat-mul [& args] (apply m/mmul args))
    (defdist dirac
        "Dirac distribution"
        [x]  ; distribution parameters
        []   ; auxiliary bindings
        (sample* [this] x)
        (observe* [this value] (if (= x value) 0.0 (- (/ 1.0 0.0)))))
    (defn mat-add [& args] (apply m/add args))
    (defn mat-transpose [& args] (apply m/transpose args))
    (defn mat-tanh [M] (m/emap tanh M))
    (defn mat-relu [M] (m/emap (fn [x] (if (> x 0) x 0)) M))
    (defn mat-repmat [M r c]
    (let [R (reduce (partial m/join-along 0) (repeat r M))]
        (reduce (partial m/join-along 1) (repeat c R))))


;;;; not used much: this is supported by the assumption that the input code must be correct
;;;; only using it for the partial-evaluation
(def ^:dynamic primitive-procedures
    "primitive procedures, do not exist in CPS form"
    (let [;; higher-order procedures cannot be primitive
          exclude '#{loop
                      map reduce
                      filter keep keep-indexed remove
                      repeatedly
                      every? not-any? some
                      every-pred some-fn
                      comp juxt partial}
          ;; runtime namespaces
          runtime-namespaces '[clojure.core anglican.runtime foppl-compiler.primitives]]
      (set (keep (fn [[k v]]
                    (when (and (not (exclude k))
                              (fn? (var-get v)))
                      k))
                  (mapcat ns-publics runtime-namespaces)))))

dirac-distribution
; (contains? primitive-procedures 'normal)