(ns foppl-compiler.compiler
  (:require [clojure.set :refer [union]])
  (:use [foppl-compiler.primitives])
  (:use [foppl-compiler.parteval])
  (:use [anglican core runtime emit stat])
  (:require [clojure.inspector :refer [atom?]]))


;;;; Interface for a rule
;;;; each read-* function has to implement this interface
;;;; (don't know how to enforce this in clojure, just using it as a guide for now)
; (defn rule [rho phi vars expr]
;   {:E _ :G _})


(defn unimplemented-rule [rho phi vars expr]
    (println "Not implemented"))


;;;; read-expr can compile an entire expression
;;;; pre-declaration of the function is required because of the cyclic dependencies
(declare read-expr)


(def empty-graph
  {:V #{}
   :E []
   :L {}
   :O {}
  }
)

;;;; merges two graphs
;;;; TODO: fix
(defn merge-graph [g1 g2]
  {
   :V (union (:V g1) (:V g2))
   :E (into (:E g1) (:E g2))   ; merge-with
   :L (union (:L g1) (:L g2))  ; merge
   :O (union (:O g1) (:O g2))  ; merge ... TODO fix: this is probably wrong
  }
)


;;;; reads a variable or a constant and replaces variables with their correspoding values
(defn read-atom [rho phi vars expr]
  {:G empty-graph
   :E (cond
        ; be careful that the vars should not contain expressions containing variables
        (contains? vars expr) (get vars expr)
        :else expr
      )
  }
)


(defn read-let-expr [rho phi vars expr]
  (let [exprs (rest expr)
        v (first (first exprs))
        e1 (first (rest (first exprs)))
        e2 (first (rest exprs))
        eval1 (read-expr rho phi vars e1)
        eval2 (read-expr rho phi (assoc vars v (:E eval1)) e2)
        ]
    {:E (:E eval2)
     :G (merge-graph (:G eval1) (:G eval2))
     }
  )
)


(defn read-if-expr [rho phi vars expr]
  (let [exprs (rest expr)
        e1 (first exprs)
        e2 (first (rest exprs))
        e3 (first (rest (rest exprs)))
        eval1 (read-expr rho phi vars e1)
        eval2 (read-expr
          rho
          (part-eval (list 'and phi (:E eval1)))
          vars
          e2
        )
        eval3 (read-expr
          rho
          (part-eval (list 'and phi (list 'not (:E eval1))))
          vars
          e3
        )
        E (part-eval (list 'if (:E eval1) (:E eval2) (:E eval3)))
        G (merge-graph (merge-graph (:G eval1) (:G eval2)) (:G eval3))
        ]
  {:E E
   :G G}
  )
)


;;;; finds the set of (free-)variables used in an expression
(defn free-vars [expr]
  ; (println "-->" (atom? expr) expr)
  (cond
    (number? expr) #{}
    (symbol? expr) #{expr}
    (atom? expr) #{}
    (vector? expr) (apply union (map free-vars expr))
    (map? expr) (apply union (map free-vars (vals expr)))
    (= (first expr) 'if)
      (union
        (union
          (free-vars (first (rest expr)))
          (free-vars (first (rest (rest expr))))
        )
        (free-vars (first (rest (rest (rest expr)))))
      )
    (seq? expr) (reduce union #{} (map free-vars (rest expr)))
    :else #{}
  )
)

;;;; creates the score function expression
;;;; this can be used to create the *log*-likelihood of the observed value
(defn score [expr v]
  ; (cond
  ;   (= 'if (first expr)) (let [
  ;       exprs (rest expr)
  ;       e1 (first exprs)
  ;       e2 (first (rest exprs))
  ;       e3 (first (rest (rest exprs)))
  ;       e3 (if (= e3 nil) '(normal 0 1) e3)
  ;     ]
  ;     (list 'if e1 (score e2 v) (score e3 v))
  ;   )
    ; :else
      (list 'observe* expr v)
  ; )
)


(defn read-sample-expr [rho phi vars expr]
  (let [e (first (rest expr))
        eval (read-expr rho phi vars e)
        evalE (:E eval)
        evalG (:G eval)
        v (gensym "S__")
        z (free-vars evalE)
        f (score evalE v)]
      {:E v
       :G {:V (conj (:V evalG) v)
           :E (into (:E evalG) (map #(list % v) z))
           :L (assoc (:L evalG) v f)
           :O (:O evalG)
       }
      }
  )
)


(defn read-observe-expr [rho phi vars expr]
  (let [exprs (rest expr)
        e1 (first exprs)
        e2 (first (rest exprs))
        eval1 (read-expr rho phi vars e1)
        eval2 (read-expr rho phi vars e2)
        G (merge-graph (:G eval1) (:G eval2))
        v (gensym "O__")
        f1 (score (:E eval1) v)
        f (list 'if phi f1 0)
        z (disj (free-vars f1) v)
        b (map #(list % v) z)
      ]
    {:E (:E eval2)
     :G {:V (conj (:V G) v)
         :E (into (:E G) b)
         :L (assoc (:L G) v f)
         :O (assoc (:O G) v (:E eval2))
     }
    }
  )
)


(def keyword-rules {
    'let read-let-expr
    'if read-if-expr
    'sample read-sample-expr
    'observe read-observe-expr
  }
)


(defn read-primitive-application [rho phi vars expr]
  (let [evals (map #(read-expr rho phi vars %) (rest expr))
        exprs (cons (first expr) (map #(:E %) evals))
        ]
    {:E (part-eval exprs)
     :G (reduce merge-graph empty-graph (map #(:G %) evals))
     }
  )
)


(defn read-function-application [rho phi vars expr]
  (let [func (get rho (first expr))
        evals (map #(read-expr rho phi vars %) (rest expr))
        G (reduce merge-graph empty-graph (map #(:G %) evals))
        evalE (read-expr
                rho
                phi
                (into
                  vars
                  (mapv
                    #(vec (list %1 %2))
                    (:args func) (map #(:E %) evals)
                  )
                )
                (:body func))
      ]
    {:E (:E evalE)
     :G (merge-graph G (:G evalE))}
  )
)


;;;; reads an entire expression which corresponds to `e` in the FOPPL grammar
(defn read-expr [rho phi vars expr]
  (let [rule (cond
               ; var or constant
               (not (seq? expr)) read-atom
               ; function
               (contains? rho (first expr)) read-function-application  ; TODO
               ; keyword
               (contains? keyword-rules (first expr)) (get keyword-rules (first expr))
               ; primitive operation application
               :else read-primitive-application
            )]
    (rule rho phi vars expr)
  )
)


;;;; reads a function definition
;;;; TODO fix: handle function application inside functions
(defn read-func [rho expr]
  (let [exprs (rest expr)
        args (first (rest exprs))
        body (first (rest (rest exprs)))
    ]
    {:name (first exprs)
     :args args
     :body body}
  )
)


;;;; reads the entire program which corresponds to `q` in the FOPPL grammar
;;;; ** please note that an extra set of parens is necessary to be able
;;;; to read the program (confirmed by Frank)
(defn read-program [rho phi vars expr]
  (cond
    (and
      (and (seq? expr) (> (count expr) 1))
      (and (seq? (first expr)) (= 'defn (first (first expr))))
    ) (let [f (read-func rho (first expr))]
        (read-program
          (assoc rho (:name f) {:args (:args f) :body (:body f)})
          phi
          vars
          (rest expr)
        )
      )
    :else (read-expr rho phi vars (first expr))
  )
)

