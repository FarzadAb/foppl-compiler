(ns foppl-compiler.sugar
  (:require [clojure.inspector :refer [atom?]]))

;;;; pre-declaring desugar because of the cyclic dependencies
(declare desugar)

;;;; desugars let
(defn desugar-let [expr]
  (let [vars (first expr)
        exprs (reverse (map desugar (rest expr)))
        desugared-exprs
          (loop [cexpr (first exprs)
                  extra (rest exprs)]
            (if (empty? extra)
              cexpr
              (recur
                (list 'let ['_ (first extra)] cexpr)
                (rest extra)
              )
            )
          )
    ]
    (loop [cexpr desugared-exprs
            extra (reverse (partition 2 vars))]
      (if (empty? extra)
        cexpr
        (recur
          (list 'let [(first (first extra)) (desugar (first (rest (first extra))))] cexpr)
          (rest extra)
        )
      )
    )
  )
)

;;;; desugars loop
(defn desugar-loop [expr]
  (let [c (eval (first expr))
        e0 (first (rest expr))
        slice (rest (rest expr))
        f (first slice)
        exprs (rest slice)
        symbols (map #(symbol (str "a" (+ % 1))) (range (count exprs)))
        varlist-init
          (vec
            (mapcat #(list %1 %2) symbols exprs)
          )
    ]
    (loop [
        i 0
        fparam e0
        varlist varlist-init
      ]
      (if (= i c)
        (desugar-let (list varlist fparam))
        (let [v (symbol (str "v" i))]
          (recur
            (+ i 1)
            v
            (conj (conj varlist v) (cons f (cons i (cons fparam symbols))))
          )
        )
      )
    )
  )
)

;;;; desugars foreach
(defn desugar-foreach [expr]
  (let [c (eval (first expr))
        vars (partition 2 (first (rest expr)))
        exprs (rest (rest expr))
        desugared
          (map
            (fn [i] (
              cons
                (vec
                  (mapcat
                    (fn [x] (list (first x) (list 'get (first (rest x)) i)))
                    vars))
                exprs
            ))
            (range c)
          )
    ]
    (cons 'vector (map desugar-let desugared))
  )
)

;;;; desugars an expression or function definition
(defn desugar [expr]
  (cond
    (vector? expr) (cons 'vector (seq (map desugar expr)))
    (map? expr) (cons 'hash-map (mapcat (fn [x] [(first x) (desugar (nth x 1))]) expr))
    (seq? expr)
      (let [f (first expr) r (rest expr)]
        (cond
          (= f 'defn) (concat (list 'defn (first r) (first (rest r))) (map desugar (rest (rest r))))
          (= f 'let) (desugar-let r)
          (= f 'loop) (desugar-loop r)
          (= f 'foreach) (desugar-foreach r)
          :else (cons f (map desugar r))
        )
      )
    :else expr
  )
)

;;;; desugars the whole program (q)
(defn desugar-program [expr]
  (map desugar expr)
)
