(ns foppl-compiler.parteval
    (:require [clojure.set :refer [union]])
    (:use [foppl-compiler.primitives])
    (:use [anglican core runtime emit stat])
    (:require [clojure.inspector :refer [atom?]]))

;;;; partial evaluation for if expressions
(defn part-eval-if [expr]
  (let [c1 (try (eval (first (rest expr))) (catch Exception e 'error))]
      (if (= c1 'error)
        expr
        (if
          (true? c1)
          (first (rest (rest expr)))
          (first (rest (rest (rest expr))))
        )
      )
    )
)

;;;; checks to see if the expr is a safe datastructure (list/map/vector)
;;;; it makes sure `expr` is not (an unevaluated) primitive procedure
(defn is-part-eval-ds [expr]
  (or
    (map? expr)
    (vector? expr)
    (and (seq? expr) (not (contains? primitive-procedures (first expr))))
  )
)

;;;; these operations only requires one argument which must be a datastructure (list/map/vector)
(def type1-ops #{
  'peek
  'first
  'second
  'rest
  'last
})

;;;; these operations only requires two argument:
;;;;   1. a datastructure (list/map/vector)
;;;;   2. a number
(def type2-ops #{
  'nth
  'get
})

;;;; these operations also requires two argument:
;;;;   1. a datastructure (list/map/vector)
;;;;   2. another object
(def type3-ops #{
  'conj
  'append
})

;;;; these operations also requires two argument:
;;;;   1. an object
;;;;   2. a datastructure (list/map/vector)
(def type4-ops #{
  'cons
})

;;;; Partially evaluates an expression.
;;;; Assumes the lower level expressions are already partially evaluated as much as possible
;;;; Also assumes `expr` is a list, since we won't call it otherwise
(defn part-eval [expr]
  ; (println "<::--::" expr (ns-resolve (find-ns 'foppl-compiler.primitives) 'append))
  (cond
    (= 'if (first expr)) (part-eval-if expr)
    (= 'list (first expr)) (rest expr)
    (= 'vector (first expr)) (vec (rest expr))
    (= 'hash-map (first expr)) (apply hash-map (rest expr))

    (and (>= (count expr) 2) (contains? type1-ops (first expr)) (is-part-eval-ds (nth expr 1)))
      (
        (ns-resolve (find-ns 'foppl-compiler.primitives) (first expr))
        (nth expr 1)
      )

    (and (>= (count expr) 3) (contains? type3-ops (first expr)) (is-part-eval-ds (nth expr 1)))
      (
        (ns-resolve (find-ns 'foppl-compiler.primitives) (first expr))
        (nth expr 1)
        (nth expr 2)
      )

    (and
      (>= (count expr) 3)
      (contains? type2-ops (first expr))
      (is-part-eval-ds (nth expr 1))
      (number? (nth expr 2))
    )
      (
        (ns-resolve (find-ns 'foppl-compiler.primitives) (first expr))
        (nth expr 1)
        (nth expr 2)
      )

    (and
      (>= (count expr) 3)
      (contains? type4-ops (first expr))
      (is-part-eval-ds (nth expr 2))
    )
      (
        (ns-resolve (find-ns 'foppl-compiler.primitives) (first expr))
        (nth expr 1)
        (nth expr 2)
      )

    (contains? #{'normal 'flip 'dirac 'discrete 'gamma 'dirichlet} (first expr)) expr

    ; assuming this is a primitive operation for now
    :else (try (eval expr) (catch Exception e expr))
  )
)