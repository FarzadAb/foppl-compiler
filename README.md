# foppl-compiler

Task description is available on the [course webpage](https://www.cs.ubc.ca/~fwood/CS532W-539W/homework/4.html).

## Examples

You can run the tests via:
```bash
lein test
```

The `core-test.clj` file does some examples of how you can compile programs and how to interpret the output.

All question programs are tested in the `core-test.clj` file under the names `program[1-4]-test`. These test evaluate the number of vertices and edges as well as some other minor features.

## Testing

### HW5

You can run the tests for homework 5 using the following command:
```bash
lein test autodiff.test-helper
```


### HW8

You can run the tests for homework 8 using the following command:
```bash
lein test hoppl.test-helper
```


## Usage

### FOPPL Compiler

You can use the `foppl-compiler.core/compile-expr` function to compile your FOPPL expressions to PGMs:
```clojure
(compile-expr '(
    (defn id [x] x)
    (id 0)
  )
)
=> {:E _ :G _}
```
The output will be a `hash-map` with entries `:E` for the final expression and `:G` for the graph. The graph format is as follows:
```clojure
{:V #{}  ; vertices as a set
 :E []   ; edges (arcs) as a vector
 :L {}   ; link functions as a hash-map (with vertices as keys)
 :O {}   ; observed variables as a hash-map (with vertices as keys)
  }
```
**Note:** the input expression must be **list of lists** where the first few items are function definitions and the last one is an expression. This does look a bit different than the homework document but it is necessary for making it work (as confirmed by Frank).

**Note:** the user is responsible for making sure the input language is correct, no error handling functionality is implemented.

### HOPPL Interpreter

You can run the HOPPL interpreter for `nb-samples` number of samples and get the empirical mean and variance estimates by running the following code:
```clojure
(hoppl.core/imp-sampling <nb-samples> <code>)
```
Where `<code>` is your code written in HOPPL. Currently we only support a small set of primitive procedures, but you can easily extend that by addin your own function to `hoppl.library/primitives`.